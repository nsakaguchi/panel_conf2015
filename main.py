#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from numpy.random import randn
import pandas as pd
from scipy import stats

from rpy2.robjects import pandas2ri
pandas2ri.activate()
from rpy2.robjects import r

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import seaborn as sns

from ggplot import *

#from ply import install_ply, X, sym_call
#install_ply(pd)

import itertools as it
mpl.rcParams['font.family']='IPAexGothic'

import functools as ft

r.data('iris')
df_iris = pandas2ri.ri2py(r['iris'])
print(df_iris.head())
#rf = r.randomForest(robjects.Formula('b~.',data=train))
#print(rf)

class DataSet:
    def __init__(self):

        data = pd.read_csv('weight.csv')

        self.cohort_name = ['A','B','C','D','E']
        self.region_name = ["北海道","東北","関東","中部",
                   "近畿","中国","四国","九州","海外"]
        rn = self.region_name
        self.region_code = dict(zip(range(1,len(rn)+1),rn))
        self.first_year = {'A':1993,'B':1997,'C':2003,'D':2008,'E':2013}

        bins = [0,2000,3000,4000,5000,6000]
        data['chrt'] = pd.cut(data.ID,bins,labels=self.cohort_name)
        data['地域'] = (
            data.Q2.map(self.region_code)
            .astype('category',categories=self.region_name,ordered=True))

        data['year'] = data.PANEL + 1992
        data['fyear'] = data.chrt.map(self.first_year)

        self.data = data


    def get_cohort(self,*chrts):
        if any(isinstance(i, list) for i in chrts):
            chrts = list(it.chain.from_iterable(chrts))

        if 'all' in chrts:
            chrts = ['A','B','C','D','E']

        data = self.data
        data_chrt = \
                    data.query('chrt in {chrts}'.format(chrts=chrts))
        return data_chrt


class SubSample:
    def __init__(self,chrts):
        self.chrts = list(chrts)
        ds = DataSet()
        self.data = ds.get_cohort(self.chrts)
        self._data = ds.get_cohort(self.chrts)
        self.fyear = sorted([ds.first_year[i] for i in self.chrts])
        self.region_name_dom =list(ds.region_name)
        self.region_name_dom.remove("海外")

    def set_panel(self,p):
        self.data = self.data[self.data.PANEL==p]

    def reset_data(self):
        self.data = self._data


    def base_piv(self,var,**kwargs):
        trgt = self.data[['year',var]]
        if "rmv" in kwargs:
            trgt = trgt[var!=kwargs[rmv]]


        trgt_piv = (
            trgt.pivot_table(
                index=var,columns='year',
                aggfunc=len,fill_value=0)
            .sort_index(axis=0,ascending=False)
        )

        if "prop" in kwargs:
            fy = self.fyear[0]
            trgt_piv = trgt_piv.apply(
                lambda s:s.astype(float)/trgt_piv[fy],axis=0)

        return trgt_piv



    def base_piv_fig(self,p,**kwargs):
        trgt_piv = p

        annot = kwargs.get('annot',True)
        fmt = kwargs.get('fmt',"d")
        mask = kwargs.get('mask',None)
        size = kwargs.get('size',6)
        fname = kwargs.get('fname',"heatmap.png")
        dpi = kwargs.get('dpi',300)

        sns.heatmap(
            trgt_piv,annot=annot,fmt=fmt,
            annot_kws={"size": size})
        plt.savefig(
            'age_distro4.png',dpi=300)


    def age_year(self,var='Q8'):
        piv = self.base_piv(var)
        return piv


    def age_year_fig(self,**kwargs):
        p = self.age_year()
        self.base_piv_fig(p=p,**kwargs)
        return self


    def region_year2(self,var='地域',rmv="海外"):
        piv = self.base_piv
        return piv



    def region_year(self):
        sdata = self.data
        region = self.data[['year','地域']]
        region = region[region.地域!='海外']
        region_piv = (
            region.pivot_table(
                index='地域',columns='year',aggfunc=len,fill_value=0)
            .reindex(self.region_name_dom)
            )
        fy = self.fyear[0]
        region_piv_prop= region_piv.apply(
            lambda s:s.astype(float)/region_piv[fy],axis=0)
        return region_piv_prop

    def region_year_fig(self):
        ry = self.region_year()
        sns.heatmap(
            ry,annot=True,fmt="0.2f",
            annot_kws={"size": 6},cbar=False)
        plt.savefig(
            'region_distroD.png',dpi=300)

        return self

    def gakureki_year(self,var="Q60SR"):
        piv = self.base_piv(var,prop=True)
        return piv



    def gakureki_year_fig(self):
        gy = self.gakureki_year()
        sns.heatmap(
            gy,annot=True,fmt="0.2f",
            annot_kws={"size": 6},cbar=False)
        plt.savefig(
            'gakureki_distroD.png',dpi=300)

        return self


class MissingTable():
    def __init__(self):
        ds = DataSet()

        miss_dict = {}
        for c in ds.cohort_name:
            chrt = ds.get_cohort(c)

            fy = ds.first_year[c]
            p = (chrt[["ID","year"]].pivot_table(index='ID',columns='year',
                    aggfunc=len,fill_value=0)
                .sort_index(axis=0,ascending=False)
                )
            pcol = ["miss"+str(i) for i,j in enumerate(list(p.columns.values))]
            p.columns = pcol
            p.reset_index(inplace=True)
            miss_dict[c] = p
        self.miss_dict = miss_dict









if __name__ == "__main__":

    #print(DataSet.cohort_name)
    chrtA = SubSample('A')
    chrtB = SubSample('B')
    chrtC = SubSample('C')
    chrtD = SubSample('D')

    #print(chrtB.gakureki_year())
    #print(chrtA.region_year_fig())

    ds = DataSet()

    miss_dict = {}
    for c in ds.cohort_name:
        chrt = ds.get_cohort(c)

        fy = ds.first_year[c]
        p = (chrt[["ID","year"]].pivot_table(index='ID',columns='year',
                aggfunc=len,fill_value=0)
            .sort_index(axis=0,ascending=False)
            )
        pcol = ["miss"+str(i) for i,j in enumerate(list(p.columns.values))]
        p.columns = pcol
        p.reset_index(inplace=True)
        miss_dict[c] = p

    chrtA.set_panel(1)
    m = miss_dict["A"]
    d = pd.merge(chrtA.data,m,on='ID')
    d.to_csv("chrtA.csv",index=False)

    chrtB.set_panel(5)
    m = miss_dict["B"]
    d = pd.merge(chrtB.data,m,on='ID')
    d.to_csv("chrtB.csv",index=False)


    chrtC.set_panel(11)
    m = miss_dict["C"]
    d = pd.merge(chrtC.data,m,on='ID')
    d.to_csv("chrtC.csv",index=False)

    chrtD.set_panel(16)
    m = miss_dict["D"]
    d = pd.merge(chrtD.data,m,on='ID')
    d.to_csv("chrtD.csv",index=False)



