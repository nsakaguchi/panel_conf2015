require 'csv'
require 'json'

require 'rubygems'
require 'mongo'



r = Dir.pwd
directories = Dir.glob("./data/*")
begin
 Dir.mkdir("temp")
rescue
end

tmp_dir = File.join(r,"temp")


directories.each do |d|
  # d -> '/csv/p1','/csv/p2',...,'/csv/p19'

  fname = File.basename(d)
  sheet = (1..5).to_a
  sheet = sheet.map {|x| fname + "_" + x.to_s + ".csv"}
  cn = fname + ".csv"
  combined_name = File.join(tmp_dir,cn)
  pnl_each = File.join(r,d) 
  Dir.chdir(pnl_each)
  puts pnl_each
  cmnd = "paste -d, #{sheet[0]} #{sheet[1]} #{sheet[2]} #{sheet[3]} #{sheet[4]}> #{combined_name}"
  Kernel.system(cmnd)
end



Dir.chdir(tmp_dir)

db = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'panel_22')
coll = db[:pnls]


#m = Mongo::Connection.new()
#db = m.drop_database('panel_22')
#db = m.db('panel_22')

#coll = db[:pnls]

Dir.glob("*").each do |file|
  reader = CSV.open(file,"r")
  header = reader.take(1)[0]
  items = Hash.new
  represents = Array.new 
  
  reader.each do |row|
    i = 0
    row.each do |item|
      
      begin
        item2 = Integer(item)
      rescue
        item2 = item
      end
    
      items[header[i].strip] = item2
      i = i+1

    end

    t1 = Marshal.load(Marshal.dump(items))
    represents.push(t1)
  end
  
  j = 0
  represents.each do |row|
    puts j
    coll.insert_one(row)
    j = j+1
  end
end  

